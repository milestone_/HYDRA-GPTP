#pragma once
#include "SCBW\api.h"
#include "SCBW\scbwdata.h"
#include "SCBW\UnitFinder.h"
#include <vector>

namespace CFlags {
	enum {
		NoInvincible=1<<0,
		NoAir=1<<1,
		NoGround=1<<2,
		NoMechanical=1<<3,
		NoOrganic=1<<4,
		NoBuilding=1<<5,
		OnlyOrganic=1<<6,
		OnlyMechanic=1<<7,
		Enemy=1<<8,
		Ally=1<<9,
		IncludingSelf=1<<10,
		FrontalArc=1<<11,
		Spellcaster=1<<12,
		HasEnergy=1<<13,
		NotSelf=1<<14,
		Shielded=1<<15,
		Enabled=1<<16,
		Powered=1<<17,
		Completed=1<<18,
	};
}

class unitRangeCount : public scbw::UnitFinderCallbackMatchInterface
{
private:
	CUnit* center;
	u32 context;//flags
	std::vector<u16> allowedUnitIds;
public:
	unitRangeCount(CUnit* center, u32 context, std::vector<u16> allowedUnitIds)
		: center(center), context(context), allowedUnitIds(allowedUnitIds) {}

	bool match(CUnit* unit) {
		if (unit == center && !(context & CFlags::IncludingSelf)) {
			return false;
		}
		if (unit == center && context & CFlags::NotSelf) {
			return false;
		}
		if (unit->status & UnitStatus::Invincible && context & CFlags::NoInvincible) {
			return false;
		}
		if (unit->status & UnitStatus::InAir && context & CFlags::NoAir) {
			return false;
		}
		if (!(unit->status & UnitStatus::InAir) && context & CFlags::NoGround) {
			return 0;
		}
		if (context & CFlags::Spellcaster) {
			if (!(units_dat::BaseProperty[unit->id] & UnitProperty::Spellcaster)) {
				return false;
			}
		}
		if (context & CFlags::HasEnergy) {
			if (unit->energy < 256) {
				return false;
			}
		}
		if (context & CFlags::Shielded) {
			if (!units_dat::ShieldsEnabled[unit->id]) {
				return false;
			}
			if (unit->shields < 256) {
				return false;
			}
		}
		if (context & CFlags::Powered) {
			if (unit->status & UnitStatus::DoodadStatesThing) {
				return false;
			}
		}
		if (context & CFlags::Completed) {
			if (!(unit->status & UnitStatus::Completed)) {
				return false;
			}
		}
		if (center) {
			if (context & CFlags::FrontalArc) {
				u16 angle = scbw::getAngle(center->position.x, center->position.y,
					unit->position.x, unit->position.y);
				u16 unit_angle = center->velocityDirection1;
				int arc = (double)45 / 1.40625;
				//90 degrees
				int delta = unit_angle - angle;
				delta = abs((delta + 128) % 256 - 128);
				if (delta > arc) {
					return false;
				}
			}
			if (scbw::isAlliedTo(unit->playerId, center->playerId) && context & CFlags::Enemy) {
				return false;
			}
			if (!scbw::isAlliedTo(unit->playerId, center->playerId) && context & CFlags::Ally) {
				return false;
			}
		}
		if (allowedUnitIds.size() > 0) {
			bool good = false;
			for (auto a : allowedUnitIds) {
				if (unit->id == a) {
					good = true;
					break;
				}
			}
			if (!good) {
				return false;
			}
		}
		if (units_dat::BaseProperty[unit->id] & UnitProperty::Organic && context & CFlags::NoOrganic)
			return false;
		if (!(units_dat::BaseProperty[unit->id] & UnitProperty::Organic) && context & CFlags::OnlyOrganic)
			return false;
		if (units_dat::BaseProperty[unit->id] & UnitProperty::Mechanical && context & CFlags::NoMechanical)
			return false;
		if (!(units_dat::BaseProperty[unit->id] & UnitProperty::Mechanical) && context & CFlags::OnlyMechanic)
			return false;
		if (units_dat::BaseProperty[unit->id] & UnitProperty::Building && context & CFlags::NoBuilding)
			return false;
		if (!(scbw::isEnabled(unit)) && context & CFlags::Enabled) {
			return false;
		}
		return true;
	}
};
bool rangeMatch(CUnit* unit, u32 r, u32 context, std::vector<u16> allowedIds);
CUnit* rangeMatchUnit(CUnit* unit, u32 r, u32 context, std::vector<u16> allowedIds);
CUnit* rangeMatchPosition(Point32 position, u32 r, u32 context, std::vector<u16> allowedIds);
u32 hydraUnitCount(CUnit* unitPtr, Point16 pos, int r, u32 flags, std::vector<u16> unitIds);
template<typename hydraLoop>
inline void hydraFinderLoop(CUnit* unitPtr, Point16 pos, int r, u32 flags, std::vector<u16> unitIds, hydraLoop l)
{
	auto condition = unitRangeCount(unitPtr, flags, unitIds);
	scbw::UnitFinder unitsInSplash(pos.x - r, pos.y - r,
		pos.x + r,pos.y + r);
	for (int i = 0; i < unitsInSplash.getUnitCount(); i++) {
		auto unit = unitsInSplash.getUnit(i);
		if (condition.match(unit)) {
			l(unit);
		}
	}
}
