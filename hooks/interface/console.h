#pragma once
#include "SCBW/structures.h"

namespace hooks {

	s16 loadRaceUI();					// 004EE2D0
	void setTextStr(s32 a1);			// 004263E0
	void injectConsoleHooks();
}